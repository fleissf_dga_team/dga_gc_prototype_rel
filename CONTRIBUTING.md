# FLEISS Software Foundation

## How to contribute to our projects

General introduction.

### Communications

How to communicate.

### For New Contributors

Guidelines for new contributors.

### Building

How to build.

### Extending

How to extend.

### Testing

How to test.

### Best Practices

The contribution's best practices.

### Reporting bugs

How to report bugs.

### Documentation

Where to find documentation.

### References

External references.
